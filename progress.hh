/**
 * @file      progress.hh
 * @author    Christian Holm Christensen <cholm@nbi.dk>
 * @date      Jun 1, 2017
 * @copyright 2021 Christian Holm Christensen
 * @license   GNU Lesser Public License
 * 
 * @brief  A progress meter 
 */
#ifndef PROGRESS_HH
#define PROGRESS_HH
#include <ctime>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>

namespace progress
{
  /** 
   * Break down duraction in hours, minutes, seconds, and
   * milliseconds.  Exists in C++20 - but made here for backward
   * compatibility.
   */
  template <typename Duration>
  struct _hh_mm_ss
  {
    _hh_mm_ss(Duration d) : _d(d) {}
    std::chrono::hours hours() const
    {
      using h=std::chrono::hours;
      return std::chrono::duration_cast<h>(_d);
    }
    std::chrono::minutes minutes() const
    {
      using m = std::chrono::minutes;
      return std::chrono::duration_cast<m>(_d-hours());
    }
    std::chrono::seconds seconds() const
    {
      using s = std::chrono::seconds;
      return std::chrono::duration_cast<s>(_d-hours()-minutes());
    }
    std::chrono::milliseconds milliseconds() const
    {
      using ms = std::chrono::milliseconds;
      return std::chrono::duration_cast<ms>(_d-hours()-minutes()-seconds());
    }
    Duration _d;
  };
  template <typename Duration>
  _hh_mm_ss<Duration> hh_mm_ss(Duration& d)
  {
    return _hh_mm_ss<Duration>(d);
  }
  template <typename Duration>
  std::ostream& operator<<(std::ostream& o, const _hh_mm_ss<Duration> &d)
  {
    return o << std::setfill(' ') << std::setw(3) << d.hours().count() << ":"
	     << std::setfill('0') << std::setw(2) << d.minutes().count() << ":"
	     << std::setfill('0') << std::setw(2) << d.seconds().count() << "."
	     << std::setfill('0') << std::setw(3) << d.milliseconds().count()
	     << std::setfill(' ');
  }
    
  //==================================================================
  /** 
   * A progress meter 
   */
  struct meter
  {
    using clock=std::chrono::high_resolution_clock;
    using point=std::chrono::time_point<clock>;

    /** When we started */
    point _start;
    /** Update frequency */
    long  _freq;
    /** Output stream */
    std::ostream& _out;
    /** 
     * Constructor 
     * 
     * @param f Update frequency
     * @param out Output stream
     */
    meter(long f=1, std::ostream& out=std::cout) :
      _start(clock::now()), _freq(f), _out(out)
    { }
    /**
     * Print current progress 
     */
    void print(long cur, long end, const char* pre="Processed")
    {
      using namespace std::chrono;
      if (_freq <= 0 or (cur % _freq) != 0) 
	return;

      auto now = clock::now();
            
      std::stringstream s;
      s.precision(3);
      s << std::left << std::setw(9) << pre << std::right
	<< std::setw(9) << cur;
      if (end > 0) s << " of " << std::setw(9) << end;

      std::chrono::duration<double> e = now - _start;
      auto elap = hh_mm_ss(e);
      s << ",   Elapsed " << elap;

      double   speed = cur / e.count();
      s << " (" << std::setw(5) << speed << "/s)";
      
      if (end > 0 && cur > 0) {
	std::chrono::duration<double> r = double(end-cur)/cur * e;
	auto rem = hh_mm_ss(r);
	s << ",   ETA " << rem;
      }

      _out << "\r" << s.str() << std::flush;
      if (cur == end) _out << std::endl;
    }
  };
}

#endif
//
// EOF
//

