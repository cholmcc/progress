#
#
#
PROGRESS_SOURCES	:= progress.hh
SOURCES			+= $(PROGRESS_SOURCES)
EXTRA_DIST		+= options/test.cc Makefile README.md options/Makefile

CXX			:= g++
CXXFLAGS		:= -c --std=c++11 --pedantic -Wall 
CPPFLAGS		:= -I.
LD			:= g++
LDFLAGS			:= 

vpath %.cc tests

%.o:%.cc
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $<

%:%.o
	$(LD) $(LDFLAGS) -o $@ $<

all:	test

clean:
	rm -f test test.o

test.o:test.cc $(SOURCES)
