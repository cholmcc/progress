#include <random>
#include <thread>
#include <progress.hh>

int main()
{
#if __cplusplus > 201402L
  using namespace std::chrono_literals;
  auto base = 1s;
#else
  std::chrono::seconds base(1);
#endif 
  
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_real_distribution<> dis(0.5, 1.5);

  int nev = 30;
  progress::meter meter(1);
  for (int i = 0; i < nev; i++) {
    std::this_thread::sleep_for(dis(gen)*base);
    meter.print(i,nev);
  }
  meter.print(nev,nev);
}
