# A simple progress meter 

```c++
#include <random>
#include <thread>
int main()
{
  using namespace std::chrono_literals;
  
  std::random_device rd;
  std::default_random_engine gen(rd());
  std::uniform_real_distribution<> dis(0.5, 1.5);

  int nev = 30;
  progress::meter meter(1);
  for (size_t i = 0; i < nev; i++) {
    std::this_thread::sleep_for(dis(gen)*1s);
    meter.print(i,nev);
  }
  meter.print(nev,nev);
}
```
